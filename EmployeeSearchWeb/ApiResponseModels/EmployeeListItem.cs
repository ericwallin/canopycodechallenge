﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.ApiResponseModels
{
    public class EmployeeListItem
    {
        #region "Private Constructor"
        private EmployeeListItem()
        {

        }
        #endregion

        #region "Factory Methods"

        private static EmployeeListItem Create()
        {
            // set any default values for blank items
            return new EmployeeListItem();
        }

        private static EmployeeListItem Create(WebHRIS.Responses.EmployeeDetailData apiData)
        {
            EmployeeListItem result = new EmployeeListItem();

            // set any default values for blank items, format items for display

            result.EmployeeNumber = apiData.EmploymentNumber;
            result.FullName =
                (apiData.PersonalSuffixName.Length == 0) ?
                    (apiData.PersonalMiddleInitial.Length == 0) ? String.Format("{0}, {1}", apiData.PersonalLastName, apiData.PersonalFirstName) : String.Format("{0}, {1} {2}.", apiData.PersonalLastName, apiData.PersonalFirstName, apiData.PersonalMiddleInitial) :
                    (apiData.PersonalMiddleInitial.Length == 0) ? String.Format("{0} {1}, {2}", apiData.PersonalLastName, apiData.PersonalSuffixName, apiData.PersonalFirstName) : String.Format("{0} {1}, {2} {3}.", apiData.PersonalLastName, apiData.PersonalSuffixName, apiData.PersonalFirstName, apiData.PersonalMiddleInitial);

            string ssn = apiData.PersonalSsn.Trim().Replace("-", "");

            if (System.Text.RegularExpressions.Regex.IsMatch(ssn, @"^\d{9}$"))
            {
                result.Ssn = String.Format("XXX-XX-{0}", ssn.Substring(5, 4));
            } else
            {
                result.Ssn = "";
            }

            result.EmpID = apiData.EmployeeUserId;
            result.Department = apiData.PositionCc2DepartmentName;
            result.JobClassification = apiData.JobClassificationNumber;
            result.PositionNumber = apiData.PositionNumber;
            result.PositionTitle = apiData.PositionName;
            result.PayrollGroup = apiData.PayrollGroupName;
            result.Status = apiData.EmploymentStatus;

            return result;
        }

        public static List<EmployeeListItem> CreateList()
        {
            return new List<EmployeeListItem>();
        }

        public static List<EmployeeListItem> CreateList(List<WebHRIS.Responses.EmployeeDetailData> apiDataList)
        {
            List<EmployeeListItem> result = new List<EmployeeListItem>();

            foreach (var apiData in apiDataList)
            {
                result.Add(EmployeeListItem.Create(apiData));
            }

            return result;
        }

        #endregion

        #region "Properties"

        public string EmployeeNumber { get; set; } = "";
        public string FullName { get; set; } = "";
        public string Ssn { get; set; } = "";
        public string EmpID { get; set; } = "";
        public string Department { get; set; } = "";
        public string JobClassification { get; set; } = "";
        public string PositionNumber { get; set; } = "";
        public string PositionTitle { get; set; } = "";
        public string PayrollGroup { get; set; } = "";
        public string Status { get; set; } = "";

        #endregion

    }
}