﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;

namespace EmployeeSearchWeb.ApiResponseModels
{
    public class EmployeeDetailItem
    {
        #region "Private Constructor"
        
        private EmployeeDetailItem()
        {

        }

        #endregion

        #region "Factory Methods"

        public static EmployeeDetailItem Create()
        {
            return new EmployeeDetailItem();
        }

        public static EmployeeDetailItem Create(WebHRIS.Responses.EmployeeDetailData apiData)
        {

            EmployeeDetailItem result = new EmployeeDetailItem();

            // (pi) Personal Information
            result.pi_FullName =
                (apiData.PersonalSuffixName.Length == 0) ?
                    (apiData.PersonalMiddleName.Length == 0) ? String.Format("{0}, {1}", apiData.PersonalLastName, apiData.PersonalFirstName) : String.Format("{0}, {1} {2}.", apiData.PersonalLastName, apiData.PersonalFirstName, apiData.PersonalMiddleName) :
                    (apiData.PersonalMiddleName.Length == 0) ? String.Format("{0} {1}, {2}", apiData.PersonalLastName, apiData.PersonalSuffixName, apiData.PersonalFirstName) : String.Format("{0} {1}, {2} {3}.", apiData.PersonalLastName, apiData.PersonalSuffixName, apiData.PersonalFirstName, apiData.PersonalMiddleName);

            string ssn = apiData.PersonalSsn.Trim().Replace("-", "");

            if (System.Text.RegularExpressions.Regex.IsMatch(ssn, @"^\d{9}$"))
            {
                result.pi_Ssn = String.Format("XXX-XX-{0}", ssn.Substring(5, 4));
            }
            else
            {
                result.pi_Ssn = "";
            }

            result.pi_Birthday = apiData.PersonalBirthday;

            // (es) Employment Status
            result.es_EmployeeNumber = apiData.EmploymentNumber;
            result.es_EmploymentStatus = apiData.EmploymentStatus;
            result.es_HireDate = (apiData.EmploymentHireDate.Length == 0) ? "--" : apiData.EmploymentHireDate;
            result.es_TerminationDate = (apiData.EmploymentTerminationDate.Length == 0) ? "--" : apiData.EmploymentTerminationDate;

            // (ci) Contact Information
            result.ci_HomeStreet1 = apiData.PersonalHomeStreet1;
            result.ci_HomeStreet2 = apiData.PersonalHomeStreet2;
            result.ci_HomeCity = apiData.PersonalHomeCity;
            result.ci_HomeState = apiData.PersonalHomeState;
            result.ci_HomeZipCode = apiData.PersonalHomeZipCode;
            result.ci_HomeCountry = apiData.PersonalHomeCountry;

            if ((apiData.PersonalHomePhoneAreaCode.Length == 3) && (apiData.PersonalHomePhoneNumber.Length == 7))
            {
                result.ci_HomePhoneDisplay = String.Format("+1 ({0}) {1}-{2}", apiData.PersonalHomePhoneAreaCode, apiData.PersonalHomePhoneNumber.Substring(0, 3), apiData.PersonalHomePhoneNumber.Substring(3, 4));
                result.ci_HomePhoneLink = String.Format("tel:+1-{0}-{1}-{2}", apiData.PersonalHomePhoneAreaCode, apiData.PersonalHomePhoneNumber.Substring(0, 3), apiData.PersonalHomePhoneNumber.Substring(3, 4));
            }
            else
            {
                result.ci_HomePhoneDisplay = String.Format("{0}{1}", apiData.PersonalHomePhoneAreaCode, apiData.PersonalHomePhoneNumber);
                result.ci_HomePhoneLink = "";
            }

            if ((apiData.PersonalCellPhoneAreaCode.Length == 3) && (apiData.PersonalCellPhoneNumber.Length == 7))
            {
                result.ci_CellPhoneDisplay = String.Format("+1 ({0}) {1}-{2}", apiData.PersonalCellPhoneAreaCode, apiData.PersonalCellPhoneNumber.Substring(0, 3), apiData.PersonalCellPhoneNumber.Substring(3, 4));
                result.ci_CellPhoneLink = String.Format("tel:+1-{0}-{1}-{2}", apiData.PersonalCellPhoneAreaCode, apiData.PersonalCellPhoneNumber.Substring(0, 3), apiData.PersonalCellPhoneNumber.Substring(3, 4));
            }
            else
            {
                result.ci_CellPhoneDisplay = String.Format("{0}{1}", apiData.PersonalCellPhoneAreaCode, apiData.PersonalCellPhoneNumber);
                result.ci_CellPhoneLink = "";
            }

            result.ci_WorkEmailDisplay = apiData.EmploymentEmail;

            if (apiData.EmploymentEmail.Length > 0)
            {
                result.ci_WorkEmailLink = String.Format("mailto:{0}", apiData.EmploymentEmail);
            }
            else
            {
                result.ci_WorkEmailLink = "";
            }

            // (ps) Position Information
            result.ps_JobClassificationNumber = apiData.JobClassificationNumber;
            result.ps_JobClassificationTitle = apiData.JobClassificationTitle;
            result.ps_JobClassificationDescription = apiData.JobClassificationDescription;
            result.ps_PositionNumber = apiData.PositionNumber;
            result.ps_PositionName = apiData.PositionName;
            result.ps_ReportsTo =
                (apiData.EmploymentReportsToPersonalMiddleInitial.Length == 0) ? String.Format("{0}, {1}", apiData.EmploymentReportsToPersonalLastName, apiData.EmploymentReportsToPersonalFirstName) : String.Format("{0}, {1} {2}.", apiData.EmploymentReportsToPersonalLastName, apiData.EmploymentReportsToPersonalFirstName, apiData.EmploymentReportsToPersonalMiddleInitial);


            // (cm) Compensation Information
            result.cm_PayEffectiveDate = apiData.PayEffectiveDate;
            result.cm_PayTypeCode = apiData.PayTypeCode;
            result.cm_PayDisplay = String.Format("{0:N} / {1:N} / {2:N}", apiData.PayAnnualized, apiData.PayPerPeriod, apiData.PayHourly);
            result.cm_PayFrequency = apiData.PayFrequencyCode;
            result.cm_PayrollGroupName = apiData.PayrollGroupName;

            result.ec_FullName = apiData.PrimaryEmergencyContactName;
            result.ec_Relationship = apiData.PrimaryEmergencyContactRelationship;
            result.ec_Street1 = apiData.PrimaryEmergencyContactAddress1;
            result.ec_Street2 = apiData.PrimaryEmergencyContactAddress2;
            result.ec_City = apiData.PrimaryEmergencyContactCity;
            result.ec_State = apiData.PrimaryEmergencyContactState;
            result.ec_Zip = apiData.PrimaryEmergencyContactZip;
            result.ec_Country = apiData.PrimaryEmergencyContactCountry;

            if ((apiData.PrimaryEmergencyContactHomePhoneAreaCode.Length == 3) && (apiData.PrimaryEmergencyContactHomePhoneNumber.Length == 7))
            {
                result.ec_HomePhoneDisplay = String.Format("+1 ({0}) {1}-{2}", apiData.PrimaryEmergencyContactHomePhoneAreaCode, apiData.PrimaryEmergencyContactHomePhoneNumber.Substring(0, 3), apiData.PrimaryEmergencyContactHomePhoneNumber.Substring(3, 4));
                result.ec_HomePhoneLink = String.Format("tel:+1-{0}-{1}-{2}", apiData.PrimaryEmergencyContactHomePhoneAreaCode, apiData.PrimaryEmergencyContactHomePhoneNumber.Substring(0, 3), apiData.PrimaryEmergencyContactHomePhoneNumber.Substring(3, 4));
            }
            else
            {
                result.ci_HomePhoneDisplay = String.Format("{0}{1}", apiData.PrimaryEmergencyContactHomePhoneAreaCode, apiData.PrimaryEmergencyContactHomePhoneNumber);
                result.ci_HomePhoneLink = "";
            }

            if ((apiData.PrimaryEmergencyContactWorkPhoneAreaCode.Length == 3) && (apiData.PrimaryEmergencyContactHomePhoneNumber.Length == 7))
            {
                result.ec_WorkPhoneDisplay = String.Format("+1 ({0}) {1}-{2}", apiData.PrimaryEmergencyContactWorkPhoneAreaCode, apiData.PrimaryEmergencyContactHomePhoneNumber.Substring(0, 3), apiData.PrimaryEmergencyContactWorkPhoneNumber.Substring(3, 4));
                result.ec_WorkPhoneLink = String.Format("tel:+1-{0}-{1}-{2}", apiData.PrimaryEmergencyContactWorkPhoneAreaCode, apiData.PrimaryEmergencyContactHomePhoneNumber.Substring(0, 3), apiData.PrimaryEmergencyContactWorkPhoneNumber.Substring(3, 4));
            }
            else
            {
                result.ec_WorkPhoneDisplay = String.Format("{0}{1}", apiData.PrimaryEmergencyContactWorkPhoneAreaCode, apiData.PrimaryEmergencyContactWorkPhoneNumber);
                result.ec_WorkPhoneLink = "";
            }

            if ((apiData.PrimaryEmergencyContactCellPhoneAreaCode.Length == 3) && (apiData.PrimaryEmergencyContactCellPhoneNumber.Length == 7))
            {
                result.ec_CellPhoneDisplay = String.Format("+1 ({0}) {1}-{2}", apiData.PrimaryEmergencyContactCellPhoneAreaCode, apiData.PrimaryEmergencyContactCellPhoneNumber.Substring(0, 3), apiData.PrimaryEmergencyContactWorkPhoneNumber.Substring(3, 4));
                result.ec_CellPhoneLink = String.Format("tel:+1-{0}-{1}-{2}", apiData.PrimaryEmergencyContactCellPhoneAreaCode, apiData.PrimaryEmergencyContactCellPhoneNumber.Substring(0, 3), apiData.PrimaryEmergencyContactWorkPhoneNumber.Substring(3, 4));
            }
            else
            {
                result.ec_CellPhoneDisplay = String.Format("{0}{1}", apiData.PrimaryEmergencyContactCellPhoneAreaCode, apiData.PrimaryEmergencyContactCellPhoneNumber);
                result.ec_CellPhoneLink = "";
            }

            result.ec_HomeEmailDisplay = apiData.PrimaryEmergencyContactHomeEmail;

            if (apiData.PrimaryEmergencyContactHomeEmail.Length > 0)
            {
                result.ec_HomeEmailLink = String.Format("mailto:{0}", apiData.PrimaryEmergencyContactHomeEmail);
            }
            else
            {
                result.ec_HomeEmailLink = "";
            }

            result.ec_WorkEmailDisplay = apiData.PrimaryEmergencyContactWorkEmail;

            if (apiData.PrimaryEmergencyContactWorkEmail.Length > 0)
            {
                result.ec_WorkEmailLink = String.Format("mailto:{0}", apiData.PrimaryEmergencyContactWorkEmail);
            }
            else
            {
                result.ec_WorkEmailLink = "";
            }

            return result;
        }

        #endregion

        #region "Properties"

        // (pi) Personal Information
        //public string pi_FirstName { get; set; } = "";
        //public string pi_MiddleInitial { get; set; } = "";
        //public string pi_LastName { get; set; } = "";
        //public string pi_NameSuffix { get; set; } = "";
        public string pi_FullName { get; set; } = "";
        public string pi_Ssn { get; set; } = "";
        public string pi_Birthday { get; set; } = "";


        // (es) Employment Status
        public string es_EmployeeNumber { get; set; } = "";
        public string es_EmploymentStatus { get; set; } = "";
        public string es_HireDate { get; set; } = "";
        public string es_TerminationDate { get; set; } = "";

        // (ci) Contact Information
        public string ci_HomeStreet1 { get; set; } = "";
        public string ci_HomeStreet2 { get; set; } = "";
        public string ci_HomeCity { get; set; } = "";
        public string ci_HomeState { get; set; } = "";
        public string ci_HomeZipCode { get; set; } = "";
        public string ci_HomeCountry { get; set; } = "";
        public string ci_HomePhoneDisplay { get; set; } = "";
        public string ci_HomePhoneLink { get; set; } = "";
        public string ci_CellPhoneDisplay { get; set; } = "";
        public string ci_CellPhoneLink { get; set; } = "";
        public string ci_WorkEmailDisplay { get; set; } = "";
        public string ci_WorkEmailLink { get; set; } = "";

        // (ps) Position Information
        public string ps_JobClassificationNumber { get; set; } = "";
        public string ps_JobClassificationTitle { get; set; } = "";
        public string ps_JobClassificationDescription { get; set; } = "";
        public string ps_PositionNumber { get; set; } = "";
        public string ps_PositionName { get; set; } = "";
        public string ps_ReportsTo { get; set; } = "";


        // (cm) Compensation Information
        public string cm_PayEffectiveDate { get; set; } = "";
        public string cm_PayTypeCode { get; set; } = "";
        public string cm_PayFrequency { get; set; } = "";
        public string cm_PayDisplay { get; set; } = "";
        public string cm_PayrollGroupName { get; set; } = "";

        // (ec) Emergency Contact
        public string ec_FullName { get; set; } = "";
        public string ec_Relationship { get; set; } = "";
        public string ec_Street1 { get; set; } = "";
        public string ec_Street2 { get; set; } = "";
        public string ec_City { get; set; } = "";
        public string ec_State { get; set; } = "";
        public string ec_Zip { get; set; } = "";
        public string ec_Country { get; set; } = "";
        public string ec_HomePhoneDisplay { get; set; } = "";
        public string ec_HomePhoneLink { get; set; } = "";
        public string ec_WorkPhoneDisplay { get; set; } = "";
        public string ec_WorkPhoneLink { get; set; } = "";
        public string ec_CellPhoneDisplay { get; set; } = "";
        public string ec_CellPhoneLink { get; set; } = "";
        public string ec_HomeEmailDisplay { get; set; } = "";
        public string ec_HomeEmailLink { get; set; } = "";
        public string ec_WorkEmailDisplay { get; set; } = "";
        public string ec_WorkEmailLink { get; set; } = "";




        #endregion

    }
}