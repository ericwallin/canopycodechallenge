﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using Nancy;
using Nancy.Localization;

namespace EmployeeSearchWeb
{
    public static class Util
    {

        public static string JSEscapeString(string value)
        {
            return value.Replace(@"\", @"\\").Replace(@"'", @"\'").Replace(@"""", @"\""");
        }

        public static string GetJSEscapedResource(string resourceName)
        {
            string result = "";

            ResourceManager rm = new ResourceManager("EmployeeSearchWeb.Resources.Home.Strings", System.Reflection.Assembly.GetExecutingAssembly());

            string resourceValue = rm.GetString(resourceName);

            if (resourceValue != null)
            {
                result = resourceValue.Replace(@"'", @"\'").Replace(@"""", @"\""");
            }

            return result;

        }
    }
}