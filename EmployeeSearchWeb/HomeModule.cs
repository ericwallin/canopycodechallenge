﻿using System.Configuration;
using System.Collections.Generic;
using EmployeeSearchWeb.Models;
using EmployeeSearchWeb.ApiResponseModels;
using Nancy;

namespace EmployeeSearchWeb
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            // returns client application web page
            Get["/"] = _ =>
            {
                return View["home", new { itemsPerPage = System.Configuration.ConfigurationManager.AppSettings.Get("ApiListItemsPerPage") }];
            };

            // Proxies call to ExportsDetailsSSNV1
            Get["/api/GetEmployeeBySSN", true] = async (_, ct) =>
            {
                ResponseModelOf<EmployeeDetailItem> result = null;
                WebHRIS.Responses.GetEmployeeDetailResult apiResult = null;
                Nancy.HttpStatusCode status;

                if (Request.Query["ssn"].HasValue)
                {
                    // clean up the SSN
                    string ssn = Request.Query["ssn"].ToString().Trim().Replace("-", "");

                    // check input to see if it is a well-formed SSN
                    if (System.Text.RegularExpressions.Regex.IsMatch(ssn, @"^\d{9}$"))
                    {
                        apiResult = await WebHRIS.ApiClient.GetEmployeeDetailBySSN(ConfigurationManager.AppSettings.Get("ApiCompanyID"), ssn);
                        if (apiResult.RecordFound)
                        {
                            // records found, return them
                            result = new ResponseModelOf<EmployeeDetailItem>(true, 1, EmployeeDetailItem.Create(apiResult.Data));
                            status = Nancy.HttpStatusCode.OK;
                        } else
                        {
                            // no records found
                            result = new ResponseModelOf<EmployeeDetailItem>(true, 0, EmployeeDetailItem.Create());
                            status = Nancy.HttpStatusCode.NotFound;
                        }
                    }
                    else
                    {
                        // invalid input
                        result = new ResponseModelOf<EmployeeDetailItem>(false, 0, EmployeeDetailItem.Create());
                        status = Nancy.HttpStatusCode.BadRequest;
                    }
                }
                else
                {
                    // ssn invalid or not provided
                    result = new ResponseModelOf<EmployeeDetailItem>(false, 0, EmployeeDetailItem.Create());
                    status = Nancy.HttpStatusCode.BadRequest;
                }

                return Response.AsJson<ResponseModelOf<EmployeeDetailItem>>(result).WithStatusCode(status);
            };

            // Proxies call to ExportsDetailsEmployeeNumberV1
            Get["/api/GetEmployeeByEmployeeNumber", true] = async (_, ct) =>
            {
                ResponseModelOf<EmployeeDetailItem> result = null;
                WebHRIS.Responses.GetEmployeeDetailResult apiResult = null;
                Nancy.HttpStatusCode status;

                if (Request.Query["employeeNumber"].HasValue)
                {
                    // cleaning up the SSN
                    string employeeNumber = Request.Query["employeeNumber"].ToString().Trim();

                    // TODO check input to see if it is a well-formed employee number
                    if (employeeNumber.Length > 0)
                    {
                        apiResult = await WebHRIS.ApiClient.GetEmployeeDetailByEmployeeNumber(ConfigurationManager.AppSettings.Get("ApiCompanyID"), employeeNumber);
                        if (apiResult.RecordFound)
                        {
                            // records found, return them
                            result = new ResponseModelOf<EmployeeDetailItem>(true, 1, EmployeeDetailItem.Create(apiResult.Data));
                            status = Nancy.HttpStatusCode.OK;
                        }
                        else
                        {
                            // no records found
                            result = new ResponseModelOf<EmployeeDetailItem>(true, 0, EmployeeDetailItem.Create());
                            status = Nancy.HttpStatusCode.NotFound;
                        }
                    }
                    else
                    {
                        // invalid input
                        result = new ResponseModelOf<EmployeeDetailItem>(false, 0, EmployeeDetailItem.Create());
                        status = Nancy.HttpStatusCode.BadRequest;
                    }
                }
                else
                {
                    // employee number invalid or not provided
                    result = new ResponseModelOf<EmployeeDetailItem>(false, 0, EmployeeDetailItem.Create());
                    status = Nancy.HttpStatusCode.BadRequest;
                }

                return Response.AsJson<ResponseModelOf<EmployeeDetailItem>>(result).WithStatusCode(status);
            };

            // Proxies call to ExportsListV1
            Get["/api/GetEmployees", true] = async (_, ct) =>
            {
                ResponseModelOf<List<EmployeeListItem>> responseValue;
                Nancy.HttpStatusCode status;

                int pageValue = -1;
                int itemsPerPageValue = -1;

                if ((Request.Query["page"].HasValue && int.TryParse(Request.Query["page"], out pageValue) && pageValue > 0) &&
                    (Request.Query["itemsPerPage"].HasValue && int.TryParse(Request.Query["itemsPerPage"], out itemsPerPageValue) && itemsPerPageValue > 0 && itemsPerPageValue < 1000))
                {
                    // page & itemsPerPage provided with conforming values                    

                    WebHRIS.Responses.GetEmployeeListResult apiResult = await WebHRIS.ApiClient.GetEmployeeList(ConfigurationManager.AppSettings.Get("ApiCompanyID"), pageValue, int.Parse(ConfigurationManager.AppSettings.Get("ApiListItemsPerPage")));

                    // return data from call
                    responseValue = new ResponseModelOf<List<EmployeeListItem>>(true, apiResult.RecordCount, EmployeeListItem.CreateList(apiResult.Data));
                    status = HttpStatusCode.OK;
                }
                else
                {
                    // request missing or has invalid page and itemsPerPage values
                    responseValue = new ResponseModelOf<List<EmployeeListItem>>(false, "{page} and {itemsPerPage} are required querystring values.");
                    status = HttpStatusCode.BadRequest;
                }

                return Response.AsJson<ResponseModelOf<List<EmployeeListItem>>>(responseValue).WithStatusCode(status);
            };
        }
    }
}