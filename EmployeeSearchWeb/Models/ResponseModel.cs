﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.Models
{
    public class ResponseModelOf<T>
    {
        public bool success { get; set; } = false;
        public string message { get; set; } = "";
        public int recordCount { get; set; } = 0;
        public T data { get; set; }
        public DateTime responseDate { get; set; } = DateTime.Now;

        public ResponseModelOf(bool successValue, int recordCountValue, T dataValue)
        {
            success = successValue;
            recordCount = recordCountValue;
            data = dataValue;
        }

        public ResponseModelOf(bool successValue, string messageValue)
        {
            success = successValue;
            message = messageValue;
        }

    }
}