﻿using Nancy.ViewEngines.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb
{
    public class RazorConfig : IRazorConfiguration
    {
        public IEnumerable<string> GetAssemblyNames()
        {
            yield return "EmployeeSearchWeb";
            yield return "System.Web, Version = 4.0.0.0, Culture = neutral, PublicKeyToken = b03f5f7f11d50a3a";
        }

        public IEnumerable<string> GetDefaultNamespaces()
        {
            yield return "EmployeeSearchWeb";
            yield return "EmployeeSearchWeb.Models";
        }

        public bool AutoIncludeModelNamespace
        {
            get { return true; }
        }
    }
}