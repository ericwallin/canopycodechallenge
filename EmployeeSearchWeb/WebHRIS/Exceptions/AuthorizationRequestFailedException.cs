﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.WebHRIS.Exceptions
{

    /// <summary>
    /// Thrown to indicate that an authorization token cannot be acquired.
    /// </summary>
    public class AuthorizationRequestFailedException : ApplicationException
    {
        public string Description { get; set; } = "";

        public System.Net.HttpStatusCode Status { get; set; }

        public string Content { get; set; } = "";

        private string _message = "";
        public override string Message
        {
            get
            {
                return _message;
            }
        }

        public AuthorizationRequestFailedException(string description, System.Net.HttpStatusCode status, string content)
        {
            Description = description;
            Status = status;
            Content = content;
            _message = String.Format(@"Unable to acquire authorization token. (Status = [{0}]}, Content = [{1}])", Status, Content);
        }
    }


}