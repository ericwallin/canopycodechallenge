﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.WebHRIS.Exceptions
{
    /// <summary>
    /// Thrown to indicate that no bearer token was able to be acquired.
    /// </summary>
    public class BearerTokenNotAvailableException : ApplicationException
    {
        public override string Message
        {
            get
            {
                return "No bearer token is available to use to authenticate to the API.";
            }
        }
    }
}