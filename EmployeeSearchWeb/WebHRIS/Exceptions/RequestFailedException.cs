﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.WebHRIS.Exceptions
{
    public class RequestFailedException : ApplicationException
    {
        public string Description { get; set; } = "";

        public System.Net.HttpStatusCode Status { get; set; }

        public string Content { get; set; } = "";

        private string _message = "";
        public override string Message
        {
            get
            {
                return _message;
            }
        }

        public RequestFailedException(string description, System.Net.HttpStatusCode status, string content)
        {
            Description = description;
            Status = status;
            Content = content;
            _message = String.Format(@"{0} (Status = [{1}]}, Content = [{2}])", Description, Status, Content);
        }
    }


}