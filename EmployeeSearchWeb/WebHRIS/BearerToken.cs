﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.WebHRIS
{
    public class BearerToken
    {
        public string message { get; set; } = "";
        public string access_token { get; set; } = "";
        public string token_type { get; set; } = "";
        public int expires_in { get; set; } = -1;
        public DateTime acquired_date { get; } = DateTime.Now;
    }
}