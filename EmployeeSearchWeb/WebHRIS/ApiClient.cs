﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Configuration;
using System.Threading.Tasks;
using Newtonsoft.Json;
using EmployeeSearchWeb.WebHRIS.Exceptions;
using EmployeeSearchWeb.WebHRIS.Responses;

namespace EmployeeSearchWeb.WebHRIS
{
    public class ApiClient
    {
        static private object __apiBearerTokenLockObject = new object();
        static private BearerToken __apiBearerToken = null;

        public static BearerToken GetBearerToken(bool forceRefresh = false)
        {
            BearerToken result = __apiBearerToken;

            if (forceRefresh || (result == null))
            {
                lock (__apiBearerTokenLockObject)
                {
                    // check to see if if there's a new __apiData object since we checked before the lock
                    if (!forceRefresh && (__apiBearerToken != null))
                    {
                        // (edge case) if a bearerToken was acquired between the "if (forceRefresh || (result == null))" and the lock,
                        // we will simply get another as it is happening inside of the lock, there should be no bad consequences.

                        // if it does now exist, assign it and be done.
                        result = __apiBearerToken;
                    }
                    else
                    {

                        // else, if there is no valid bearer token, acquire one.

                        RestClient client = new RestClient(ConfigurationManager.AppSettings.Get("ApiBaseUrl"));
                        RestRequest req = new RestRequest("auth/token/");
                        req.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                        req.AddParameter("application/x-www-form-urlencoded", String.Format(@"username={0}&password={1}&grant_type={2}", HttpUtility.UrlEncode(ConfigurationManager.AppSettings.Get("ApiUsername")), HttpUtility.UrlEncode(ConfigurationManager.AppSettings.Get("ApiPassword")), "password"), ParameterType.RequestBody);

                        IRestResponse response = null;
                        try
                        {
                            response = client.Post(req);
                        }
                        catch (Exception e)
                        {
                            throw new ApplicationException(String.Format("Exception thrown while requesting a bearer token. (Message = [{0}])", e.Message));
                        }

                        switch (response.StatusCode)
                        {
                            case System.Net.HttpStatusCode.OK:

                                try
                                {
                                    if (response.ContentLength > 0)
                                    {
                                        __apiBearerToken = JsonConvert.DeserializeObject<BearerToken>(response.Content);

                                        if (__apiBearerToken.access_token.Trim().Length == 0)
                                        {
                                            // access_token is blank, no good.
                                            throw new AuthorizationRequestFailedException("Access token is empty.", response.StatusCode, response.Content);
                                        }
                                        else
                                        {
                                            result = __apiBearerToken;
                                        }
                                    }
                                    else
                                    {
                                        // no content was returned in the response, no token.
                                        throw new AuthorizationRequestFailedException("No content was returned from the server.", response.StatusCode, response.Content);
                                    }
                                }
                                catch
                                {
                                    // deserialization error, there is no token.
                                    throw new AuthorizationRequestFailedException("Content from server could not be deserialized.", response.StatusCode, response.Content);
                                }
                                break;
                            default:
                                // if a token wasn't issued, we're done here.
                                throw new AuthorizationRequestFailedException("There was an error when requesting a bearer token.", response.StatusCode, response.Content);
                        }
                    }
                }
            }

            return result;
        }

        // sample SSN: 123121234
        public static async Task<GetEmployeeDetailResult> GetEmployeeDetailBySSN(string company, string ssn)
        {
            GetEmployeeDetailResult result = new GetEmployeeDetailResult();
            BearerToken token = null;

            token = GetBearerToken();

            if (token != null)
            {
                // token acquired, make the api call
                RestClient client = new RestClient(ConfigurationManager.AppSettings.Get("ApiBaseUrl"));
                IRestRequest req = new RestRequest(String.Format("v1/company/{0}/exports/ssn/{1}", HttpUtility.UrlEncode(company.Trim()), HttpUtility.UrlEncode(ssn.Trim())));
                req.AddHeader("Authorization", String.Format("bearer {0}", token.access_token));

                IRestResponse response = null;
                response = await client.ExecuteGetTaskAsync(req);

                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.OK:
                        result.RecordFound = true;
                        result.Data = JsonConvert.DeserializeObject<EmployeeDetailData>(response.Content);

                        break;
                    case System.Net.HttpStatusCode.InternalServerError:
                        // when records don't exist, it appears the server returns a 500
                        result.RecordFound = false;
                        result.Data = new EmployeeDetailData();

                        break;
                    case System.Net.HttpStatusCode.Unauthorized:
                        // token has expired during runtime, invalidate the token and re-run the call.
                        token = GetBearerToken(true);

                        client = new RestClient(ConfigurationManager.AppSettings.Get("ApiBaseUrl"));
                        req = new RestRequest(String.Format("v1/company/{0}/exports/ssn/{1}", HttpUtility.UrlEncode(company.Trim()), HttpUtility.UrlEncode(ssn.Trim())));
                        req.AddHeader("Authorization", String.Format("bearer {0}", token.access_token));

                        response = await client.ExecuteGetTaskAsync(req);

                        switch (response.StatusCode)
                        {
                            case System.Net.HttpStatusCode.OK:
                                result.RecordFound = true;
                                result.Data = JsonConvert.DeserializeObject<EmployeeDetailData>(response.Content);

                                break;
                            case System.Net.HttpStatusCode.InternalServerError:
                                // when records don't exist, it appears the server returns a 500
                                result.RecordFound = false;
                                result.Data = new EmployeeDetailData();

                                break;
                            case System.Net.HttpStatusCode.Unauthorized:
                                // using fresh bearer token, must be some other problem if still unauthorized, give up.
                                throw new RequestFailedException("API call to get employee details using SSN was unauthorized.", response.StatusCode, response.Content);
                            default:
                                throw new RequestFailedException("API call to get employee details using SSN failed.", response.StatusCode, response.Content);
                        }

                        break;
                    default:
                        throw new RequestFailedException("API call to get employee details using SSN failed.", response.StatusCode, response.Content);
                }
            }
            else
            {
                // can't get data without a valid token
                throw new BearerTokenNotAvailableException();
            }

            return result;
        }

        // sample employee number: JLBC1234
        public static async Task<GetEmployeeDetailResult> GetEmployeeDetailByEmployeeNumber(string company, string employeeNumber)
        {
            GetEmployeeDetailResult result = new GetEmployeeDetailResult();
            BearerToken token = null;

            token = GetBearerToken();

            if (token != null)
            {

                // token acquired, make the api call
                RestClient client = new RestClient(ConfigurationManager.AppSettings.Get("ApiBaseUrl"));
                IRestRequest req = new RestRequest(String.Format("v1/company/{0}/exports/employee-number/{1}", HttpUtility.UrlEncode(company.Trim()), HttpUtility.UrlEncode(employeeNumber.Trim())));
                req.AddHeader("Authorization", String.Format("bearer {0}", token.access_token));

                IRestResponse response = null;
                response = await client.ExecuteGetTaskAsync(req);

                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.OK:
                        result.RecordFound = true;
                        result.Data = JsonConvert.DeserializeObject<EmployeeDetailData>(response.Content);

                        break;
                    case System.Net.HttpStatusCode.InternalServerError:
                        // when records don't exist, it appears the server returns a 500
                        result.RecordFound = false;
                        result.Data = new EmployeeDetailData();

                        break;
                    case System.Net.HttpStatusCode.Unauthorized:
                        // token has expired during runtime, invalidate the token and re-run the call.
                        token = GetBearerToken(true);

                        client = new RestClient(ConfigurationManager.AppSettings.Get("ApiBaseUrl"));
                        req = new RestRequest(String.Format("v1/company/{0}/exports/employee-number/{1}", HttpUtility.UrlEncode(company.Trim()), HttpUtility.UrlEncode(employeeNumber.Trim())));
                        req.AddHeader("Authorization", String.Format("bearer {0}", token.access_token));

                        response = await client.ExecuteGetTaskAsync(req);

                        switch (response.StatusCode)
                        {
                            case System.Net.HttpStatusCode.OK:
                                result.RecordFound = true;
                                result.Data = JsonConvert.DeserializeObject<EmployeeDetailData>(response.Content);

                                break;
                            case System.Net.HttpStatusCode.InternalServerError:
                                // when records don't exist, it appears the server returns a 500
                                result.RecordFound = false;
                                result.Data = new EmployeeDetailData();

                                break;
                            case System.Net.HttpStatusCode.Unauthorized:
                                // using fresh bearer token, must be some other problem if still unauthorized, give up.
                                throw new RequestFailedException("API call to get employee details using employee number was unauthorized.", response.StatusCode, response.Content);
                            default:
                                throw new RequestFailedException("API call to get employee details using employee number failed.", response.StatusCode, response.Content);
                        }

                        break;
                    default:
                        throw new RequestFailedException("API call to get employee details using employee number failed.", response.StatusCode, response.Content);
                }
            }
            else
            {
                // can't get data without a valid token
                throw new BearerTokenNotAvailableException();
            }

            return result;
        }

        public static async Task<GetEmployeeListResult> GetEmployeeList(string company, int pageId, int itemsPerPage)
        {
            GetEmployeeListResult result = new GetEmployeeListResult();
            BearerToken token = null;

            token = GetBearerToken();

            if (token != null)
            {

                // token acquired, make the api call
                RestClient client = new RestClient(ConfigurationManager.AppSettings.Get("ApiBaseUrl"));
                IRestRequest req = new RestRequest(String.Format("v1/company/{0}/exports/?pagingParameterModel.pageNumber={1}&pagingParameterModel.pageSize={2}", HttpUtility.UrlEncode(company.Trim()), pageId, itemsPerPage));
                req.AddHeader("Authorization", String.Format("bearer {0}", token.access_token));

                IRestResponse response = null;
                response = await client.ExecuteGetTaskAsync(req);

                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.OK:
                        result.Data = JsonConvert.DeserializeObject<List<EmployeeDetailData>>(response.Content);
                        result.RecordCount = result.Data.Count;

                        break;
                    case System.Net.HttpStatusCode.InternalServerError:
                        // when records don't exist, it appears the server returns a 500
                        result.Data = new List<EmployeeDetailData>();
                        result.RecordCount = 0;

                        break;
                    case System.Net.HttpStatusCode.Unauthorized:
                        // token has expired during runtime, invalidate the token and re-run the call.
                        token = GetBearerToken(true);

                        client = new RestClient(ConfigurationManager.AppSettings.Get("ApiBaseUrl"));
                        req = new RestRequest(String.Format("v1/company/{0}/exports/?pagingParameterModel.pageNumber={1}&pagingParameterModel.pageSize={2}", HttpUtility.UrlEncode(company.Trim()), pageId, itemsPerPage));
                        req.AddHeader("Authorization", String.Format("bearer {0}", token.access_token));

                        response = await client.ExecuteGetTaskAsync(req);

                        switch (response.StatusCode)
                        {
                            case System.Net.HttpStatusCode.OK:
                                result.Data = JsonConvert.DeserializeObject<List<EmployeeDetailData>>(response.Content);
                                result.RecordCount = result.Data.Count;

                                break;
                            case System.Net.HttpStatusCode.InternalServerError:
                                // when records don't exist, it appears the server returns a 500
                                result.Data = new List<EmployeeDetailData>();
                                result.RecordCount = 0;

                                break;
                            case System.Net.HttpStatusCode.Unauthorized:
                                // using fresh bearer token, must be some other problem if still unauthorized, give up.
                                throw new RequestFailedException("API call to get employee list was unauthorized.", response.StatusCode, response.Content);
                            default:
                                throw new RequestFailedException("API call to get employee list failed.", response.StatusCode, response.Content);
                        }

                        break;
                    default:
                        throw new RequestFailedException("API call to get employee list failed.", response.StatusCode, response.Content);
                }
            }
            else
            {
                // can't get data without a valid token
                throw new BearerTokenNotAvailableException();
            }

            return result;

        }

    }
}