﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.WebHRIS.Responses
{

    public class GetEmployeeListResult
    {
        public int RecordCount { get; set; } = 0;
        public List<EmployeeDetailData> Data { get; set; } = new List<EmployeeDetailData>();
    }
}