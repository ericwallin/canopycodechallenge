﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.WebHRIS.Responses
{

    public class GetEmployeeDetailResult
    {
        public bool RecordFound { get; set; } = false;
        public EmployeeDetailData Data { get; set; } = null;
    }

}