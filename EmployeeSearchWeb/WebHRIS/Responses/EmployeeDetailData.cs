﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeSearchWeb.WebHRIS.Responses
{
    public class EmployeeDetailData
    {
        public int EmployeeCompanyId { get; set; } = 0; // 236,
        public string EmployeeUserId { get; set; } = ""; // "98eba962-fec3-4242-8667-f2efd801633b",

        private string _PersonalFirstName = "";
        public string PersonalFirstName
        {
            get
            {
                return _PersonalFirstName.Trim();
            }
            set
            {
                _PersonalFirstName = value;
            }
        } // "",
        private string _PersonalMiddleName = "";
        public string PersonalMiddleName
        {
            get
            {
                return _PersonalMiddleName.Trim();
            }
            set
            {
                _PersonalMiddleName = value;
            }
        } // "",
        private string _PersonalMiddleInitial = "";
        public string PersonalMiddleInitial
        {
            get
            {
                return _PersonalMiddleInitial.Trim();
            }
            set
            {
                _PersonalMiddleInitial = value;
            }
        } // "",
        private string _PersonalLastName = "";
        public string PersonalLastName
        {
            get
            {
                return _PersonalLastName.Trim();
            }
            set
            {
                _PersonalLastName = value;
            }
        } // "Campbell",
        private string _PersonalSuffixName = "";
        public string PersonalSuffixName
        {
            get
            {
                return _PersonalSuffixName.Trim();
            }
            set
            {
                _PersonalSuffixName = value;
            }
        } // "",
        private string _PersonalSuffixDescription = "";
        public string PersonalSuffixDescription
        {
            get
            {
                return _PersonalSuffixDescription.Trim();
            }
            set
            {
                _PersonalSuffixDescription = value;
            }
        } // "",
        private string _PersonalSsn = "";
        public string PersonalSsn
        {
            // mask the SSN so that nobody can scrape the results page for all the SSNs
            get
            {
                return _PersonalSsn.Trim();
            }
            set
            {
                _PersonalSsn = value;
            }
        } // "123121234",
        private string _PersonalBirthday = "";
        public string PersonalBirthday
        {
            get
            {
                return _PersonalBirthday.Trim();
            }
            set
            {
                _PersonalBirthday = value;
            }
        } // "07/19/1994",
        private string _PersonalHomeStreet1 = "";
        public string PersonalHomeStreet1
        {
            get
            {
                return _PersonalHomeStreet1.Trim();
            }
            set
            {
                _PersonalHomeStreet1 = value;
            }
        } // "1234 Street",
        private string _PersonalHomeStreet2 = "";
        public string PersonalHomeStreet2
        {
            get
            {
                return _PersonalHomeStreet2.Trim();
            }
            set
            {
                _PersonalHomeStreet2 = value;
            }
        } // "",
        private string _PersonalHomeCity = "";
        public string PersonalHomeCity
        {
            get
            {
                return _PersonalHomeCity.Trim();
            }
            set
            {
                _PersonalHomeCity = value;
            }
        } // "Atlanta",
        private string _PersonalHomeState = "";
        public string PersonalHomeState
        {
            get
            {
                return _PersonalHomeState.Trim();
            }
            set
            {
                _PersonalHomeState = value;
            }
        } // "GA",
        private string _PersonalHomeZipCode = "";
        public string PersonalHomeZipCode
        {
            get
            {
                return _PersonalHomeZipCode.Trim();
            }
            set
            {
                _PersonalHomeZipCode = value;
            }
        } // "30303",
        private string _PersonalHomeCountry = "";
        public string PersonalHomeCountry
        {
            get
            {
                return _PersonalHomeCountry.Trim();
            }
            set
            {
                _PersonalHomeCountry = value;
            }
        } // "USA",
        private string _PersonalSex = "";
        public string PersonalSex
        {
            get
            {
                return _PersonalSex.Trim();
            }
            set
            {
                _PersonalSex = value;
            }
        } // "Male",
        private string _PersonalMaritalStatus = "";
        public string PersonalMaritalStatus
        {
            get
            {
                return _PersonalMaritalStatus.Trim();
            }
            set
            {
                _PersonalMaritalStatus = value;
            }
        } // "Single",
        private string _PersonalRaceEthnicity = "";
        public string PersonalRaceEthnicity
        {
            get
            {
                return _PersonalRaceEthnicity.Trim();
            }
            set
            {
                _PersonalRaceEthnicity = value;
            }
        } // "Black or African American",
        private string _PersonalHomePhoneAreaCode = "";
        public string PersonalHomePhoneAreaCode
        {
            get
            {
                return _PersonalHomePhoneAreaCode.Trim();
            }
            set
            {
                _PersonalHomePhoneAreaCode = value;
            }
        } // "",
        private string _PersonalHomePhoneNumber = "";
        public string PersonalHomePhoneNumber
        {
            get
            {
                return _PersonalHomePhoneNumber.Trim();
            }
            set
            {
                _PersonalHomePhoneNumber = value;
            }
        } // "",
        private string _PersonalCellPhoneAreaCode = "";
        public string PersonalCellPhoneAreaCode
        {
            get
            {
                return _PersonalCellPhoneAreaCode.Trim();
            }
            set
            {
                _PersonalCellPhoneAreaCode = value;
            }
        } // "",
        private string _PersonalCellPhoneNumber = "";
        public string PersonalCellPhoneNumber
        {
            get
            {
                return _PersonalCellPhoneNumber.Trim();
            }
            set
            {
                _PersonalCellPhoneNumber = value;
            }
        } // "",
        private string _PersonalIsSmoker = "";
        public string PersonalIsSmoker
        {
            get
            {
                return _PersonalIsSmoker.Trim();
            }
            set
            {
                _PersonalIsSmoker = value;
            }
        } // "N",
        private string _PersonalIsDisabledVet = "";
        public string PersonalIsDisabledVet
        {
            get
            {
                return _PersonalIsDisabledVet.Trim();
            }
            set
            {
                _PersonalIsDisabledVet = value;
            }
        } // "N",
        private string _PersonalIsVietnamEraVet = "";
        public string PersonalIsVietnamEraVet
        {
            get
            {
                return _PersonalIsVietnamEraVet.Trim();
            }
            set
            {
                _PersonalIsVietnamEraVet = value;
            }
        } // "N",
        private string _PersonalDriversLicenseNumber = "";
        public string PersonalDriversLicenseNumber
        {
            get
            {
                return _PersonalDriversLicenseNumber.Trim();
            }
            set
            {
                _PersonalDriversLicenseNumber = value;
            }
        } // "",
        private string _PersonalDriversLicenseExpiration = "";
        public string PersonalDriversLicenseExpiration
        {
            get
            {
                return _PersonalDriversLicenseExpiration.Trim();
            }
            set
            {
                _PersonalDriversLicenseExpiration = value;
            }
        } // "",
        private string _PersonalDriversLicenseState = "";
        public string PersonalDriversLicenseState
        {
            get
            {
                return _PersonalDriversLicenseState.Trim();
            }
            set
            {
                _PersonalDriversLicenseState = value;
            }
        } // "",
        private string _PersonalDriversLicenseCountry = "";
        public string PersonalDriversLicenseCountry
        {
            get
            {
                return _PersonalDriversLicenseCountry.Trim();
            }
            set
            {
                _PersonalDriversLicenseCountry = value;
            }
        } // "",
        private string _PersonalForeignCitizen = "";
        public string PersonalForeignCitizen
        {
            get
            {
                return _PersonalForeignCitizen.Trim();
            }
            set
            {
                _PersonalForeignCitizen = value;
            }
        } // "N",
        private string _JobClassificationNumber = "";
        public string JobClassificationNumber
        {
            get
            {
                return _JobClassificationNumber.Trim();
            }
            set
            {
                _JobClassificationNumber = value;
            }
        } // "",
        private string _JobClassificationTitle = "";
        public string JobClassificationTitle
        {
            get
            {
                return _JobClassificationTitle.Trim();
            }
            set
            {
                _JobClassificationTitle = value;
            }
        } // "Software Engineer1",
        private string _JobClassificationDescription = "";
        public string JobClassificationDescription
        {
            get
            {
                return _JobClassificationDescription.Trim();
            }
            set
            {
                _JobClassificationDescription = value;
            }
        } // "Software Engineer",
        private string _PositionNumber = "";
        public string PositionNumber
        {
            get
            {
                return _PositionNumber.Trim();
            }
            set
            {
                _PositionNumber = value;
            }
        } // "2345",
        private string _PositionName = "";
        public string PositionName
        {
            get
            {
                return _PositionName.Trim();
            }
            set
            {
                _PositionName = value;
            }
        } // "Software Engineer",
        private string _PositionCc1LocationName = "";
        public string PositionCc1LocationName
        {
            get
            {
                return _PositionCc1LocationName.Trim();
            }
            set
            {
                _PositionCc1LocationName = value;
            }
        } // "Atlanta",
        private string _PositionCc1LocationDescription = "";
        public string PositionCc1LocationDescription
        {
            get
            {
                return _PositionCc1LocationDescription.Trim();
            }
            set
            {
                _PositionCc1LocationDescription = value;
            }
        } // "Atlanta",
        private string _PositionCc2DepartmentName = "";
        public string PositionCc2DepartmentName
        {
            get
            {
                return _PositionCc2DepartmentName.Trim();
            }
            set
            {
                _PositionCc2DepartmentName = value;
            }
        } // "Technology",
        private string _PositionCc2DepartmentDescription = "";
        public string PositionCc2DepartmentDescription
        {
            get
            {
                return _PositionCc2DepartmentDescription.Trim();
            }
            set
            {
                _PositionCc2DepartmentDescription = value;
            }
        } // "Technology",
        private string _PositionCc3DivisionName = "";
        public string PositionCc3DivisionName
        {
            get
            {
                return _PositionCc3DivisionName.Trim();
            }
            set
            {
                _PositionCc3DivisionName = value;
            }
        } // "",
        private string _PositionCc3DivisionDescription = "";
        public string PositionCc3DivisionDescription
        {
            get
            {
                return _PositionCc3DivisionDescription.Trim();
            }
            set
            {
                _PositionCc3DivisionDescription = value;
            }
        } // "",
        private string _PositionCc4Name = "";
        public string PositionCc4Name
        {
            get
            {
                return _PositionCc4Name.Trim();
            }
            set
            {
                _PositionCc4Name = value;
            }
        } // "",
        private string _PositionCc4Description = "";
        public string PositionCc4Description
        {
            get
            {
                return _PositionCc4Description.Trim();
            }
            set
            {
                _PositionCc4Description = value;
            }
        } // "",
        private string _PositionCc5Name = "";
        public string PositionCc5Name
        {
            get
            {
                return _PositionCc5Name.Trim();
            }
            set
            {
                _PositionCc5Name = value;
            }
        } // "",
        private string _PositionCc5Description = "";
        public string PositionCc5Description
        {
            get
            {
                return _PositionCc5Description.Trim();
            }
            set
            {
                _PositionCc5Description = value;
            }
        } // "",
        private string _PositionFlsaName = "";
        public string PositionFlsaName
        {
            get
            {
                return _PositionFlsaName.Trim();
            }
            set
            {
                _PositionFlsaName = value;
            }
        } // "",
        private string _PositionFlsaDescription = "";
        public string PositionFlsaDescription
        {
            get
            {
                return _PositionFlsaDescription.Trim();
            }
            set
            {
                _PositionFlsaDescription = value;
            }
        } // "",
        private string _PositionWorkersCompName = "";
        public string PositionWorkersCompName
        {
            get
            {
                return _PositionWorkersCompName.Trim();
            }
            set
            {
                _PositionWorkersCompName = value;
            }
        } // "",
        private string _PositionWorkersCompDescription = "";
        public string PositionWorkersCompDescription
        {
            get
            {
                return _PositionWorkersCompDescription.Trim();
            }
            set
            {
                _PositionWorkersCompDescription = value;
            }
        } // "",
        private string _EmploymentNumber = "";
        public string EmploymentNumber
        {
            get
            {
                return _EmploymentNumber.Trim();
            }
            set
            {
                _EmploymentNumber = value;
            }
        } // "JLBC1234",
        private string _EmploymentStatus = "";
        public string EmploymentStatus
        {
            get
            {
                return _EmploymentStatus.Trim();
            }
            set
            {
                _EmploymentStatus = value;
            }
        } // "Active",
        private string _EmploymentTypeCode = "";
        public string EmploymentTypeCode
        {
            get
            {
                return _EmploymentTypeCode.Trim();
            }
            set
            {
                _EmploymentTypeCode = value;
            }
        } // "",
        private string _EmploymentTypeDescription = "";
        public string EmploymentTypeDescription
        {
            get
            {
                return _EmploymentTypeDescription.Trim();
            }
            set
            {
                _EmploymentTypeDescription = value;
            }
        } // "",
        private string _EmploymentHireDate = "";
        public string EmploymentHireDate
        {
            get
            {
                return _EmploymentHireDate.Trim();
            }
            set
            {
                _EmploymentHireDate = value;
            }
        } // "06/01/2018",
        private string _EmploymentOriginalHireDate = "";
        public string EmploymentOriginalHireDate
        {
            get
            {
                return _EmploymentOriginalHireDate.Trim();
            }
            set
            {
                _EmploymentOriginalHireDate = value;
            }
        } // "",
        private string _EmploymentSeniorityDate = "";
        public string EmploymentSeniorityDate
        {
            get
            {
                return _EmploymentSeniorityDate.Trim();
            }
            set
            {
                _EmploymentSeniorityDate = value;
            }
        } // "06/01/2018",
        private string _EmploymentTerminationDate = "";
        public string EmploymentTerminationDate
        {
            get
            {
                return _EmploymentTerminationDate.Trim();
            }
            set
            {
                _EmploymentTerminationDate = value;
            }
        } // "",
        private string _EmploymentEmail = "";
        public string EmploymentEmail
        {
            get
            {
                return _EmploymentEmail.Trim();
            }
            set
            {
                _EmploymentEmail = value;
            }
        } // "freddy.jimenez@canopyws.com",
        private string _EmploymentReportsToPersonalFirstName = "";
        public string EmploymentReportsToPersonalFirstName
        {
            get
            {
                return _EmploymentReportsToPersonalFirstName.Trim();
            }
            set
            {
                _EmploymentReportsToPersonalFirstName = value;
            }
        } // "Roy",
        private string _EmploymentReportsToPersonalMiddleName = "";
        public string EmploymentReportsToPersonalMiddleName
        {
            get
            {
                return _EmploymentReportsToPersonalMiddleName.Trim();
            }
            set
            {
                _EmploymentReportsToPersonalMiddleName = value;
            }
        } // "",
        private string _EmploymentReportsToPersonalMiddleInitial = "";
        public string EmploymentReportsToPersonalMiddleInitial
        {
            get
            {
                return _EmploymentReportsToPersonalMiddleInitial.Trim();
            }
            set
            {
                _EmploymentReportsToPersonalMiddleInitial = value;
            }
        } // "",
        private string _EmploymentReportsToPersonalLastName = "";
        public string EmploymentReportsToPersonalLastName
        {
            get
            {
                return _EmploymentReportsToPersonalLastName.Trim();
            }
            set
            {
                _EmploymentReportsToPersonalLastName = value;
            }
        } // "Turner",
        private string _EmploymentTerminationReasonName = "";
        public string EmploymentTerminationReasonName
        {
            get
            {
                return _EmploymentTerminationReasonName.Trim();
            }
            set
            {
                _EmploymentTerminationReasonName = value;
            }
        } // "",
        private string _EmploymentTerminationReasonDescription = "";
        public string EmploymentTerminationReasonDescription
        {
            get
            {
                return _EmploymentTerminationReasonDescription.Trim();
            }
            set
            {
                _EmploymentTerminationReasonDescription = value;
            }
        } // "",
        private string _EmploymentIsVeteran = "";
        public string EmploymentIsVeteran
        {
            get
            {
                return _EmploymentIsVeteran.Trim();
            }
            set
            {
                _EmploymentIsVeteran = value;
            }
        } // "N",
        private string _EmploymentOkToReHire = "";
        public string EmploymentOkToReHire
        {
            get
            {
                return _EmploymentOkToReHire.Trim();
            }
            set
            {
                _EmploymentOkToReHire = value;
            }
        } // "N",
        private string _PrimaryEmergencyContactName = "";
        public string PrimaryEmergencyContactName
        {
            get
            {
                return _PrimaryEmergencyContactName.Trim();
            }
            set
            {
                _PrimaryEmergencyContactName = value;
            }
        } // "Ronald Campbell          ",
        private string _PrimaryEmergencyContactRelationship = "";
        public string PrimaryEmergencyContactRelationship
        {
            get
            {
                return _PrimaryEmergencyContactRelationship.Trim();
            }
            set
            {
                _PrimaryEmergencyContactRelationship = value;
            }
        } // "Sibling                                                                                                                                                                                                                                                        ",
        private string _PrimaryEmergencyContactAddress1 = "";
        public string PrimaryEmergencyContactAddress1
        {
            get
            {
                return _PrimaryEmergencyContactAddress1.Trim();
            }
            set
            {
                _PrimaryEmergencyContactAddress1 = value;
            }
        } // "                            ",
        private string _PrimaryEmergencyContactAddress2 = "";
        public string PrimaryEmergencyContactAddress2
        {
            get
            {
                return _PrimaryEmergencyContactAddress2.Trim();
            }
            set
            {
                _PrimaryEmergencyContactAddress2 = value;
            }
        } // "                            ",
        private string _PrimaryEmergencyContactCity = "";
        public string PrimaryEmergencyContactCity
        {
            get
            {
                return _PrimaryEmergencyContactCity.Trim();
            }
            set
            {
                _PrimaryEmergencyContactCity = value;
            }
        } // "                            ",
        private string _PrimaryEmergencyContactState = "";
        public string PrimaryEmergencyContactState
        {
            get
            {
                return _PrimaryEmergencyContactState.Trim();
            }
            set
            {
                _PrimaryEmergencyContactState = value;
            }
        } // "",
        private string _PrimaryEmergencyContactCountry = "";
        public string PrimaryEmergencyContactCountry
        {
            get
            {
                return _PrimaryEmergencyContactCountry.Trim();
            }
            set
            {
                _PrimaryEmergencyContactCountry = value;
            }
        } // "",
        private string _PrimaryEmergencyContactZip = "";
        public string PrimaryEmergencyContactZip
        {
            get
            {
                return _PrimaryEmergencyContactZip.Trim();
            }
            set
            {
                _PrimaryEmergencyContactZip = value;
            }
        } // "          ",
        private string _PrimaryEmergencyContactHomePhoneAreaCode = "";
        public string PrimaryEmergencyContactHomePhoneAreaCode
        {
            get
            {
                return _PrimaryEmergencyContactHomePhoneAreaCode.Trim();
            }
            set
            {
                _PrimaryEmergencyContactHomePhoneAreaCode = value;
            }
        } // "303",
        private string _PrimaryEmergencyContactHomePhoneNumber = "";
        public string PrimaryEmergencyContactHomePhoneNumber
        {
            get
            {
                return _PrimaryEmergencyContactHomePhoneNumber.Trim();
            }
            set
            {
                _PrimaryEmergencyContactHomePhoneNumber = value;
            }
        } // "1231234",
        private string _PrimaryEmergencyContactWorkPhoneAreaCode = "";
        public string PrimaryEmergencyContactWorkPhoneAreaCode
        {
            get
            {
                return _PrimaryEmergencyContactWorkPhoneAreaCode.Trim();
            }
            set
            {
                _PrimaryEmergencyContactWorkPhoneAreaCode = value;
            }
        } // "",
        private string _PrimaryEmergencyContactWorkPhoneNumber = "";
        public string PrimaryEmergencyContactWorkPhoneNumber
        {
            get
            {
                return _PrimaryEmergencyContactWorkPhoneNumber.Trim();
            }
            set
            {
                _PrimaryEmergencyContactWorkPhoneNumber = value;
            }
        } // "",
        private string _PrimaryEmergencyContactWorkPhoneExt = "";
        public string PrimaryEmergencyContactWorkPhoneExt
        {
            get
            {
                return _PrimaryEmergencyContactWorkPhoneExt.Trim();
            }
            set
            {
                _PrimaryEmergencyContactWorkPhoneExt = value;
            }
        } // "",
        private string _PrimaryEmergencyContactCellPhoneAreaCode = "";
        public string PrimaryEmergencyContactCellPhoneAreaCode
        {
            get
            {
                return _PrimaryEmergencyContactCellPhoneAreaCode.Trim();
            }
            set
            {
                _PrimaryEmergencyContactCellPhoneAreaCode = value;
            }
        } // "",
        private string _PrimaryEmergencyContactCellPhoneNumber = "";
        public string PrimaryEmergencyContactCellPhoneNumber
        {
            get
            {
                return _PrimaryEmergencyContactCellPhoneNumber.Trim();
            }
            set
            {
                _PrimaryEmergencyContactCellPhoneNumber = value;
            }
        } // "",
        private string _PrimaryEmergencyContactHomeEmail = "";
        public string PrimaryEmergencyContactHomeEmail
        {
            get
            {
                return _PrimaryEmergencyContactHomeEmail.Trim();
            }
            set
            {
                _PrimaryEmergencyContactHomeEmail = value;
            }
        } // "",
        private string _PrimaryEmergencyContactWorkEmail = "";
        public string PrimaryEmergencyContactWorkEmail
        {
            get
            {
                return _PrimaryEmergencyContactWorkEmail.Trim();
            }
            set
            {
                _PrimaryEmergencyContactWorkEmail = value;
            }
        } // "",
        private string _TaxWorkState = "";
        public string TaxWorkState
        {
            get
            {
                return _TaxWorkState.Trim();
            }
            set
            {
                _TaxWorkState = value;
            }
        } // "GA ",
        private string _TaxResidenceState = "";
        public string TaxResidenceState
        {
            get
            {
                return _TaxResidenceState.Trim();
            }
            set
            {
                _TaxResidenceState = value;
            }
        } // "GA ",
        private string _TaxFederalMarriedStatusName = "";
        public string TaxFederalMarriedStatusName
        {
            get
            {
                return _TaxFederalMarriedStatusName.Trim();
            }
            set
            {
                _TaxFederalMarriedStatusName = value;
            }
        } // "Single",
        private string _TaxFederalMarriedStatusCode = "";
        public string TaxFederalMarriedStatusCode
        {
            get
            {
                return _TaxFederalMarriedStatusCode.Trim();
            }
            set
            {
                _TaxFederalMarriedStatusCode = value;
            }
        } // "S",
        public decimal TaxFederalExtra { get; set; } = (decimal)0.0; // 0.0,
        private string _TaxFederalExtraIsPercent = "";
        public string TaxFederalExtraIsPercent
        {
            get
            {
                return _TaxFederalExtraIsPercent.Trim();
            }
            set
            {
                _TaxFederalExtraIsPercent = value;
            }
        } // "N",
        public int TaxFederalDependents { get; set; } = 0; // 1,
        private string _TaxStateMarriedStatusName = "";
        public string TaxStateMarriedStatusName
        {
            get
            {
                return _TaxStateMarriedStatusName.Trim();
            }
            set
            {
                _TaxStateMarriedStatusName = value;
            }
        } // "Single",
        private string _TaxStateMarriedStatusCode = "";
        public string TaxStateMarriedStatusCode
        {
            get
            {
                return _TaxStateMarriedStatusCode.Trim();
            }
            set
            {
                _TaxStateMarriedStatusCode = value;
            }
        } // "A",
        public decimal TaxStateExtra { get; set; } = (decimal)0.0; // 0.0,
        private string _TaxStateExtraIsPercent = "";
        public string TaxStateExtraIsPercent
        {
            get
            {
                return _TaxStateExtraIsPercent.Trim();
            }
            set
            {
                _TaxStateExtraIsPercent = value;
            }
        } // "N",
        public int TaxStateDependents { get; set; } = 0; // 1,
        private string _TaxLocalMarriedStatusName = "";
        public string TaxLocalMarriedStatusName
        {
            get
            {
                return _TaxLocalMarriedStatusName.Trim();
            }
            set
            {
                _TaxLocalMarriedStatusName = value;
            }
        } // "",
        private string _TaxLocalMarriedStatusCode = "";
        public string TaxLocalMarriedStatusCode
        {
            get
            {
                return _TaxLocalMarriedStatusCode.Trim();
            }
            set
            {
                _TaxLocalMarriedStatusCode = value;
            }
        } // "",
        public decimal TaxLocalExtra { get; set; } = (decimal)0.0; // 0.0,
        private string _TaxLocalExtraIsPercent = "";
        public string TaxLocalExtraIsPercent
        {
            get
            {
                return _TaxLocalExtraIsPercent.Trim();
            }
            set
            {
                _TaxLocalExtraIsPercent = value;
            }
        } // "N",
        public int TaxLocalDependents { get; set; } = 0; // 0,
        private string _PayEffectiveDate = "";
        public string PayEffectiveDate
        {
            get
            {
                return _PayEffectiveDate.Trim();
            }
            set
            {
                _PayEffectiveDate = value;
            }
        } // "06/01/2018",
        private string _PayChangeReasonCode = "";
        public string PayChangeReasonCode
        {
            get
            {
                return _PayChangeReasonCode.Trim();
            }
            set
            {
                _PayChangeReasonCode = value;
            }
        } // "New Hire                                                                                            ",
        private string _PayChangeReasonDescription = "";
        public string PayChangeReasonDescription
        {
            get
            {
                return _PayChangeReasonDescription.Trim();
            }
            set
            {
                _PayChangeReasonDescription = value;
            }
        } // "New Hire                                                                                                                                                                                                                                                       ",
        private string _PayTypeCode = "";
        public string PayTypeCode
        {
            get
            {
                return _PayTypeCode.Trim();
            }
            set
            {
                _PayTypeCode = value;
            }
        } // "Full-Time                                                                                           ",
        private string _PayTypeDescription = "";
        public string PayTypeDescription
        {
            get
            {
                return _PayTypeDescription.Trim();
            }
            set
            {
                _PayTypeDescription = value;
            }
        } // "Full-Time                                                                                                                                                                                                                                                      ",
        private string _PayFrequencyCode = "";
        public string PayFrequencyCode
        {
            get
            {
                return _PayFrequencyCode.Trim();
            }
            set
            {
                _PayFrequencyCode = value;
            }
        } // "BW1976",
        private string _PayFrequencyDescription = "";
        public string PayFrequencyDescription
        {
            get
            {
                return _PayFrequencyDescription.Trim();
            }
            set
            {
                _PayFrequencyDescription = value;
            }
        } // "Bi Weekly",
        public int PayFrequencyStandardHours { get; set; } = 0; // 1976,
        public int PayFrequencyFactor { get; set; } = 0; // 26,
        private string _PayCompensationPerCode = "";
        public string PayCompensationPerCode
        {
            get
            {
                return _PayCompensationPerCode.Trim();
            }
            set
            {
                _PayCompensationPerCode = value;
            }
        } // "Annually",
        private string _PayCompensationPerDescription = "";
        public string PayCompensationPerDescription
        {
            get
            {
                return _PayCompensationPerDescription.Trim();
            }
            set
            {
                _PayCompensationPerDescription = value;
            }
        } // "Annually                                                                                                                                                                                                                                                       ",
        private string _PayCompensationPerStandardHours = "";
        public string PayCompensationPerStandardHours
        {
            get
            {
                return _PayCompensationPerStandardHours.Trim();
            }
            set
            {
                _PayCompensationPerStandardHours = value;
            }
        } // 2080,
        public int PayCompensationPerFactor { get; set; } = 0; // 1,
        private string _PayCompensation = "";
        public string PayCompensation
        {
            get
            {
                return _PayCompensation.Trim();
            }
            set
            {
                _PayCompensation = value;
            }
        } // "35000",
        public decimal PayAnnualized { get; set; } = (decimal)0.0; // 35000.0,
        public decimal PayPerPeriod { get; set; } = (decimal)0.0; // 33256.08,
        public decimal PayHourly { get; set; } = (decimal)0.0; // 16.83,
        private string _PayPayCodeName = "";
        public string PayPayCodeName
        {
            get
            {
                return _PayPayCodeName.Trim();
            }
            set
            {
                _PayPayCodeName = value;
            }
        } // "",
        private string _PayPayCodeDescription = "";
        public string PayPayCodeDescription
        {
            get
            {
                return _PayPayCodeDescription.Trim();
            }
            set
            {
                _PayPayCodeDescription = value;
            }
        } // "",
        private string _PayPayStatusName = "";
        public string PayPayStatusName
        {
            get
            {
                return _PayPayStatusName.Trim();
            }
            set
            {
                _PayPayStatusName = value;
            }
        } // "",
        private string _PayPayStatusDescription = "";
        public string PayPayStatusDescription
        {
            get
            {
                return _PayPayStatusDescription.Trim();
            }
            set
            {
                _PayPayStatusDescription = value;
            }
        } // "",
        private string _PayrollGroupName = "";
        public string PayrollGroupName
        {
            get
            {
                return _PayrollGroupName.Trim();
            }
            set
            {
                _PayrollGroupName = value;
            }
        } // "Atlanta GA",
        private string _PayrollGroupDescription = "";
        public string PayrollGroupDescription
        {
            get
            {
                return _PayrollGroupDescription.Trim();
            }
            set
            {
                _PayrollGroupDescription = value;
            }
        } // "Atlanta GA Payroll Group",
        private string _TimberlineSoftwareSicCodeCode = "";
        public string TimberlineSoftwareSicCodeCode
        {
            get
            {
                return _TimberlineSoftwareSicCodeCode.Trim();
            }
            set
            {
                _TimberlineSoftwareSicCodeCode = value;
            }
        } // "",
        private string _TimberlineSoftwareSicCodeDescription = "";
        public string TimberlineSoftwareSicCodeDescription
        {
            get
            {
                return _TimberlineSoftwareSicCodeDescription.Trim();
            }
            set
            {
                _TimberlineSoftwareSicCodeDescription = value;
            }
        } // ""
    }
}