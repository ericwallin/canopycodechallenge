﻿# Canopy Code Challenge - Eric Wallin

## Introduction

This is my take on the Canopy Code Challenge.  The goal is to have a simple and elegant search page that "just works" without frivolous distractions.

### My application uses these frameworks:

+ *Nancy Server Framework* - a simple alternative to ASP.NET/MVC, great for prototyping
+ *Vue Javascript client framework* - Popular Javascript client application framework
+ *axiom* - HTTP Javascript library for making RESTful service calls to the hosted application
+ *Skeleton* - a minimal CSS framework to make simple things look nice easily.
+ *FontAwesome* - font-based icon set
+ *RestSharp* - C# library to make asynchronous RESTful service calls from hosted API to WebHRIS API.
+ *Json.NET by Newtonsoft* - popular JSON library

## User Experience

### Application supports the following features:

+ Auto-loads paged list of employees at application start
+ Search bar for searching for an employee by Employee Number or by SSN
+ Employee name is linked to auto-search that employee's Employee Number to see employee detail
+ Telephone numbers and emails are hyperlinked
+ Paging buttons to browse data with
+ Page number box for direct page number entry. Can be used to jump to a page directly by typing in the number. Invalid page number entries auto-reset to last known good page number.
+ Shortcut keys for changing pages pages quickly
  - Left Arrow (BACK)
  - Right Arrow (FORWARD)
  - Up Arrow (FIRST PAGE)

## Application Design

The web client is built with the Vue framework, using axios to make web calls to the server portion of the application.

The application hosts 1 HTML page, and hosts 3 calls that serve as proxy wrappers for calls to the WebHRIS API.

All calls to ExportsListV1, ExportsDetailSSNV1, and ExportsDetailEmployeeNumberV1 first check to see if a valid bearer token has been acquired by the application.  If there is no bearer token, a call is made to the Authentication method to acquire one.

Upon web service call failure (presumably from expired token), one attempt to reacquire a new token is performed before repeating the data web service call to the WebHRIS API.

Data returned from the WebHRIS calls use binding features of Json.NET to bind the values to a strongly-typed data object instance.  The fields needed from this data object are reformatted and put into a different data object only containing the fields needed for display, and this data object is sent to the client.

Proxying the WebHRIS calls allows the client to rely on HTTP status codes sent from the proxy, 200 (OK), 404 (Not Found) - for searches (SSN & Employee Number) with no results, and 400 (Bad Request) for poorly formatted searches (invalid SSN in search), to help simplify the client logic.

Upon loading the client in browser, the client calls "/api/GetEmployees?pageId=1&itemsPerPage=12" to get data from the ExportsListV1 WebHRIS web method.  The GetEmployees API call only returns the fields to be displayed, and reformats those fields as needed for the UI requirements (trimming strings, Xing out SSNs, combining first, middle, last names for full name display, etc.).  Censoring the SSNs also makes it more difficult to en masse copy/paste sensitive user data from the web page.  Proxying this data also reduces the network & memory footprint of the client, making the page load more responsive.

## Required Steps to Run Application Locally

1. Clone the repo: git clone https://ericwallin@bitbucket.org/ericwallin/canopycodechallenge.git
1. Edit the Web.config and enter the WebHRIS credentials along with the CompanyID.
1. Restore the Nuget Packages.
1. Run!

## TODOs (things I'd do before pushing this to Production)

### User features I would add:

1. Apply more responsive design refinements.  The table format is already somewhat adaptable to screen size, but with multiple CSS styles for different screen sizes, I'd hide or reformat some of the columns to make the screen display more appropriately for different screen sizes.1. Implement Vue router to get back button and browser history working properly.  The detail page boxes are not uniformly sized, something easily fixed with responsive style sheets.
1. Implement wait spinners while API calls are being made and screens are refreshing
1. Implement API call error handling.  Currently, if the API calls are not working properly, you're going to have a bad time.
1. Remove the "Search by Employee Number" and "Search by SSN" radio buttons and just have a unified search box.  If it's 9 numeric digits, assume it's SSN, if not found, fall back to Employee Number search unless a more rigid Employee Number format is determined.
1. Have a more comprehensive Employee Detail view.  Given time constraints, I would prefer more informed field display choice and placement could be done better.
1. Implement a print-friendly view for the Employee Detail.
1. Hotlink Employee Detail views so links could be shared between logged in users to each employee's page.
1. CSS-based popups instead of Javascript alerts.

### Backend implementation features I would add:

1. Convert the Vue app to use Vue components.
1. Implement Vue routing to support browser history.
1. Implement HTTPS endpoint for site.
1. Convert to ASP.NET/MVC
1. Create secure login with user sessions hosted in Redis or similar cache.
1. Lock down the hosted API calls to users with valid logged in sessions.
1. Implement localization, using multilingual translations of the Strings resources for appropriate audiences.
1. Thoroughly document the design of the application with more source code comments.
1. Error & trace logging.
1. Test!

### WebHRIS implementation features I would add:

1. Whitespace trimmed values
1. More verbose error reporting
1. More liberal use of HTTP status codes in responses.  The only two responses appear to be 200 (you've got data!), and 500 (for errors and anything that doesn't return a value).

## Known Issues

1. The WebHRIS ExportsListV1 call appears to have a duplicate record bug.
1. Vue routing has not been implemented, so back button / browser history isn't working properly.

Copyright 2018 Eric Wallin